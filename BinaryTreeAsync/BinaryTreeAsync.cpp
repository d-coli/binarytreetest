
#include "pch.h"
#include <iostream>
#include <vector>
#include <future>

#include<stdio.h>
#include<stdlib.h>
#define bool int

using namespace std;

//Node struct
struct Node
{
	int data;
	Node * left;
	Node * right;
};

//Task class, determines whether the binary tree is balanced or not
class Task
{
public:
	bool isBalanced(struct Node *root, int* height)
	{
		/* lh --> Height of left subtree
		   rh --> Height of right subtree */
		int lh = 0, rh = 0;

		/* l will be true if left subtree is balanced
		  and r will be true if right subtree is balanced */
		int l = 0, r = 0;

		if (root == NULL)
		{
			*height = 0;
			return 1;
		}

		/* Get the heights of left and right subtrees in lh and rh
		  And store the returned values in l and r */
		l = isBalanced(root->left, &lh);
		r = isBalanced(root->right, &rh);

		/* Height of current node is max of heights of left and
		   right subtrees plus 1*/
		*height = (lh > rh ? lh : rh) + 1;

		/* If difference between heights of left and right
		   subtrees is more than 2 then this node is not balanced
		   so return 0 */
		if ((lh - rh >= 2) || (rh - lh >= 2))
			return 0;

		/* If this node is balanced and left and right subtrees
		  are balanced then return true */
		else return l && r;
	}

	

};

/* Helper function that allocates a new node with the
given data and NULL left and right pointers. */
struct Node* newNode(int data)
{
	struct Node* node = (struct Node*)
		malloc(sizeof(struct Node));
	node->data = data;
	node->left = NULL;
	node->right = NULL;

	return (node);
}

class Coordinator
{
public:
	// Adds a task to be managed by the Coordinator;
	// Returns true if successful, false otherwise
	bool AddTask()
	{
		try
		{
			int height = 0;
			Task t;
			Node *root = newNode(1);
			root->left = newNode(2);
			root->right = newNode(2);
			root->left->left = newNode(3);
			root->left->right = newNode(3);
			root->left->left->left = newNode(4);
			root->left->left->right = newNode(4);

			std::cout << std::endl;
			auto futureFunction = std::async([&t, root, &height] {return t.isBalanced(root, &height); });
			
			if (futureFunction.get() == 1)
				printf("Tree is balanced");
			else
				printf("Tree is not balanced");

			std::cout << std::endl;

			return true;

		}
		catch (const std::exception& e)
		{
			std::cout << e.what();
			return false;
		}
		
	}

	// Should be called by a managed Task when it is complete
	void TaskComplete() 
	{
		printf("Task Complete");
	}

	// Must be called regularly by a Task during Task::Update; 
	// Returns true if Task::Update should return immediately.
	// Returns false if okay to keep running
	//bool TaskShouldYield(const Task * task);

};



int main()
{
	Coordinator c;

	try
	{
		c.AddTask();
		c.TaskComplete();
	}
	catch (const std::exception& e)
	{
		std::cout << e.what();
	}

	return 0;
}